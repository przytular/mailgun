#! /usr/bin/python

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class MailGun(object):

    def __init__(self):
        self.smtp = None
        self.msg = None

    def connect(self, host, user, password, ssl=False):
        self.smtp = smtplib.SMTP(host)
        if ssl:
            self.smtp.ehlo()
            self.smtp.starttls()
        self.smtp.login(user, password)

    def send_msg(self, sender, recipient, subject, msg_txt, msg_html=None):
        self.msg = MIMEMultipart('alternative')
        self.msg['Subject'] = subject
        self.msg['From'] = sender
        self.msg['To'] = recipient

        # Attach plain text body
        self.msg.attach(MIMEText(msg_txt, 'plain', _charset="utf-8"))

        # Attach HTML body
        if msg_html:
            self.msg.attach(MIMEText(msg_html, 'html', _charset="utf-8"))

        self.smtp.sendmail(sender, recipient, self.msg.as_string())

    def quit(self):
        self.smtp.quit()
